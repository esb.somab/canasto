#include "Arduino.h"
#include <Wire.h>
#include <SPI.h>




//IR
int LED = 13;
int obstaclePin = 12; // sensor counting cloths
int hasObstacle = HIGH; // variable IR sensor
// Door
const int door = 11;// magnetic_sensor
int door_state; // fisical door state
int door_status;// door status 1 for open 0 for close


// Funtions
void IR();
void Door_Sensor();
void setup(){
	Serial.begin(9600);   // Initiate a serial communication

	pinMode(LED, OUTPUT);
	pinMode(obstaclePin, INPUT);
	pinMode(door, INPUT_PULLUP);

	//NFC



}//setup
void loop() {
	//IR();
	//Door_Sensor();
}//loop

void IR(){// funtion for the basic code IR_Sensor
	hasObstacle = digitalRead(obstaclePin);
	if (hasObstacle == LOW)
	{

		digitalWrite(LED, HIGH);
	}
	else
	{

		digitalWrite(LED, LOW);
	}
	delay(200);

}//final IR

void Door_Sensor(){

	door_state = digitalRead(door);

	if (door_state == HIGH){
		door_status = 1;
	}
	else{
		door_status = 0;
	}
	delay(300);


}//final sensor door

